import org.kohsuke.github.GHApp;

import java.security.cert.Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Pg {
    public static void main(String[] args) throws Exception{

        // Change GHApp to your class
        Certificate[] certificates = GHApp.class.getProtectionDomain().getCodeSource().getCertificates();

        System.out.println("Number of certificates: " + certificates.length);
        int certNumber = 1;

        for (Certificate certificate: certificates){

            System.out.println("Certificate " + certNumber++);

            // Extract from & to dates from certificate
            Pattern pattern = Pattern.compile("Validity:.*\n.*]", Pattern.MULTILINE);
            Matcher matcher = pattern.matcher(certificate.toString());
            matcher.find();
            String validity = matcher.group();
            //System.out.println(validity);

            // Parse expiration date

            String to = validity.substring(validity.indexOf("To: ")+4, validity.length()-1);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EE MMM d HH:mm:ss z yyyy");

            Date expirationDate = simpleDateFormat.parse(to);

            // Calculate number of days before expiration

            Date todayDate = new Date();
            long diff = TimeUnit.DAYS.convert((expirationDate.getTime() - todayDate.getTime()), TimeUnit.MILLISECONDS);

            System.out.println("Expiration date: " + expirationDate);
            System.out.println("Today's date: "+ new Date());
            System.out.println("Number of days left before expiration: " + diff);

        }

    }
}
